import csv
import os
import datetime

array_all = []
# array_tag = []
# array_bar = []
header = []
array_test = []
array_train = []

# file_exists = os.path.exists("../images/test/P_20191203_120025.jpg")
# print(file_exists)
i = 0
with open('all.csv') as f:
    reader = csv.reader(f)
    for row in reader:
        
        if (os.path.exists("../images/test/" + row[0])):
            array_test.append(row)
            # print('test')
            # i = i + 1
        elif (os.path.exists("../images/train/" + row[0])):
            # print('train')
            array_train.append(row)
            # i = i + 1
        else: 
            header = row

    # print(i)



array_test_w = []
array_train_w = []


array_test_w.append(header)
array_train_w.append(header)

for row in array_test:
    array_test_w.append(row)

for row in array_train:
    array_train_w.append(row)

with open('test_labels.csv', 'w') as f:
    writer = csv.writer(f)
    for row in array_test_w:
        writer.writerow(row)

with open('train_labels.csv', 'w') as f:
    writer = csv.writer(f)
    for row in array_train_w:
        writer.writerow(row)