import csv
import cv2
import os, glob

def csv_writer(data, path):
    """
    Write data to a CSV file path
    """
    with open(path, "w", newline='') as csv_file:
        writer = csv.writer(csv_file, delimiter=',')
        for line in data:
            writer.writerow(line)

data = [["filename","width","height","class","xmin","ymin","xmax","ymax"]]

def csv_dict_reader(file_obj):
    """
    Read a CSV file using csv.DictReader
    """
    reader = csv.DictReader(file_obj, delimiter=',')
    for line in reader:
        # print(line["filename"]),
        # print(line["width"])
        # print(line["height"]),
        # print(line["class"])
        # print(line["xmin"]),
        # print(line["ymin"])
        # print(line["xmax"]),
        # print(line["ymax"])

        cange_csv = 1
        if cange_csv:
            filename = line["filename"]
            width = int(int(line["width"])/2)
            height = int(int(line["height"])/2)
            class_ = line["class"]
            xmin = int(int(line["xmin"])/2)
            ymin = int(int(line["ymin"])/2)
            xmax = int(int(line["xmax"])/2)
            ymax = int(int(line["ymax"])/2)

            line = [filename,width,height,class_,xmin,ymin,xmax,ymax]
            data.append(line)
            # print(data)

    # csv_writer("annotations/train_labels.csv", data )

    with open("annotations/train_labels.csv", "w") as csv_file:
        writer = csv.writer(csv_file, delimiter=',')
        for line in data:
            writer.writerow(line)



def resize_img():

    resize_status = 0

    if resize_status:
        name = "/media/ivliev/9016-4EF8/airport_all/workspace/training_demo/images/train/" + filename
        print(name)
        img = cv2.imread(name)
        print(img.shape)

        # scale_percent = 60 # percent of original saize
        width = int(img.shape[1] / 2)
        height = int(img.shape[0] / 2)

        dim = (width, height)
        # resize image
        resized = cv2.resize(img, dim, interpolation = cv2.INTER_AREA)
        cv2.imwrite(name,resized)



# os.chdir('images/test')
# for file in glob.glob("*.jpg"):
#     # print(file)
#     filename = file 
#     # name = "images/test/" + filename
#     name = filename
#     print(name)
#     img = cv2.imread(name)
#     print(img.shape)

#     # scale_percent = 60 # percent of original saize
#     width = int(img.shape[1] / 2)
#     height = int(img.shape[0] / 2)

#     dim = (width, height)
#     # resize image
#     resized = cv2.resize(img, dim, interpolation = cv2.INTER_AREA)
#     cv2.imwrite(name,resized)

if __name__ == "__main__":
    with open("/media/ivliev/9016-4EF8/airport_all/workspace/training_demo/annotations/train_labels.csv") as f_obj:
        csv_dict_reader(f_obj)

