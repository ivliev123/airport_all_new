import csv
import os
import datetime

array_all = []
# array_tag = []
# array_bar = []
header = []
array_test = []
array_train = []



with open('annotations/test_labels.csv') as f:
    reader = csv.reader(f)
    for row in reader:
        if row[0] != 'filename': 
            # print(row)
            array_test.append(row)
        else:
            header = row


with open('annotations/train_labels.csv') as f:
    reader = csv.reader(f)
    for row in reader:
        if row[0] != 'filename': 
            # print(row)
            array_train.append(row)
        else:
            header = row

array_test.sort(key=lambda i: i[0])
print(len(array_test))

array_train.sort(key=lambda i: i[0])
print(len(array_train))

# print(array_test)
# for row in array_test:
#     print(row)

array_test_w = []
array_train_w = []


array_test_w.append(header)
array_train_w.append(header)

for row in array_test:
    array_test_w.append(row)

for row in array_train:
    array_train_w.append(row)

with open('test_labels.csv', 'w') as f:
    writer = csv.writer(f)
    for row in array_test_w:
        writer.writerow(row)

with open('train_labels.csv', 'w') as f:
    writer = csv.writer(f)
    for row in array_train_w:
        writer.writerow(row)