import os
import sys
sys.path.append("/home/ivliev/TensorFlow/models/research/object_detection/")

from utils import label_map_util

PATH_TO_LABELS = os.path.join('annotations', 'label_map.pbtxt')

print(PATH_TO_LABELS)

label_map = label_map_util.load_labelmap(PATH_TO_LABELS)
categories = label_map_util.convert_label_map_to_categories(label_map, max_num_classes=NUM_CLASSES, use_display_name=True)
category_index = label_map_util.create_category_index(categories)

print(label_map)
print(categories)
print(category_index)