import numpy as np
import os
import six.moves.urllib as urllib
import sys
import tarfile
import tensorflow as tf
import zipfile
import pickle
import imutils


# from tensorflow.keras.preprocessing.image import ImageDataGenerator
# from tensorflow.keras.models import Sequential
# from tensorflow.keras.layers import Dense, Dropout, Activation, Flatten
# from tensorflow.keras.layers import Conv2D, MaxPooling2D

from collections import defaultdict
from io import StringIO
# from matplotlib import pyplot as plt
from PIL import Image
import math
# import load_class
import cv2

import io
# import socket
import struct
import time
import zlib
import copy



# img_counter = 0
# encode_param = [int(cv2.IMWRITE_JPEG_QUALITY), 50]



cap = cv2.VideoCapture(0)

# This is needed since the notebook is stored in the object_detection folder.
# sys.path.append("..")
sys.path.append("/home/ivliev/TensorFlow/models/research/object_detection/")

from object_detection.utils import label_map_util
from object_detection.utils import visualization_utils as vis_util

sys.path.append("..")


def drow_line(image, size, color, rho, angles_array):
  try:
    for angle in angles_array:
      rho = rho
      theta = angle
      a = np.cos(theta)
      b = np.sin(theta)
      x0 = a*rho
      y0 = b*rho
      x1 = int(x0 + 1000*(-b))
      y1 = int(y0 + 1000*(a))
      x2 = int(x0 - 1000*(-b))
      y2 = int(y0 - 1000*(a))

      # print(theta*180 / np.pi)
      cv2.line(image,(x1,y1),(x2,y2),color,size)

    # cv2.imwrite('test_barcode/houghlines3.jpg',image)

  except:
    print('line_error')


def lines_filter(image):
  good_steps = 0
  now_lines_angle = []
  last_lines_angle = []
  out_lines_angle = []

  for step in range(7):
    len_line = 100 - step * 10
    lines = cv2.HoughLines(image,1,np.pi/180,len_line)
    try:
      if len(lines) > 5:
        # print(len(lines))
        now_lines_angle = []
        for line in lines:
          theta = round(line[0][1], 2) 
          now_lines_angle.append(theta)

        good_steps = good_steps + 1 

        #пересечение
        if(len(last_lines_angle) > 0):
          now_lines_angle = list(set(now_lines_angle) & set(last_lines_angle))
          # print(now_lines_angle)
          last_lines_angle = copy.deepcopy(now_lines_angle)
        else:
          last_lines_angle = copy.deepcopy(now_lines_angle)

    except:
      print("len = 0")

    if(good_steps >= 3):
      break

  return now_lines_angle

def sort_90(angles_array):
  pick = 0 
  angles_array = sorted(angles_array)
  for i in range(len(angles_array)-1):
    d_angle = angles_array[i+1] - angles_array[i]
    # print(d_angle)

    if d_angle > 1.4:
      pick = 1
    if pick:
      angles_array[i+1] = angles_array[i+1]  - 1.57 
  
  angles_array = np.array(angles_array)
  angle_out = np.mean(angles_array)

  # print(angles_array)
  # print(angle_out)
  return angle_out




def line_detaction(image):
  time_line = time.time()
  image = imutils.resize(image, width=100)

  gray = cv2.cvtColor(image,cv2.COLOR_BGR2GRAY)
  # cv2.imwrite('test_barcode/gray.jpg',gray)
  th2 = cv2.adaptiveThreshold(gray,255,cv2.ADAPTIVE_THRESH_GAUSSIAN_C, cv2.THRESH_BINARY_INV,3,3)
  # cv2.imwrite('test_barcode/th2.jpg',th2)

  ddepth = cv2.cv.CV_32F if imutils.is_cv2() else cv2.CV_32F

  gradX = cv2.Sobel(gray, ddepth=ddepth, dx=1, dy=0, ksize=-1)
  for y in range(gradX.shape[0]):
    for x in range(gradX.shape[1]):
        if gradX[y][x] <0:
          gradX[y][x] = 0
  gradX = cv2.convertScaleAbs(gradX)
  gradX = cv2.threshold(np.array(gradX),200,255,cv2.THRESH_BINARY)[1]

  gradY = cv2.Sobel(gray, ddepth=ddepth, dx=0, dy=1, ksize=-1)
  for y in range(gradY.shape[0]):
    for x in range(gradY.shape[1]):
        if gradY[y][x] <0:
          gradY[y][x] = 0
  gradY = cv2.convertScaleAbs(gradY)
  gradY = cv2.threshold(np.array(gradY),200,255,cv2.THRESH_BINARY)[1]
  # print(gradY)
  
  # cv2.imwrite("test_barcode/gradX.png",gradX)
  # cv2.imwrite("test_barcode/gradY.png",gradY)


  angles_array_1 = lines_filter(th2)
  angles_array_2 = lines_filter(gradY)
  angles_array_3 = lines_filter(gradX)

  angles_array_for_sort_90 = angles_array_1 + angles_array_2 + angles_array_3

  angle_out = sort_90(angles_array_for_sort_90)
  print('angle_out : ', angle_out)

  rho = 0
  drow_line(image, 2, (0,0,255), rho, angles_array_1)
  rho = 7
  drow_line(image, 1, (0,255,0), rho, angles_array_2)
  rho = 14
  drow_line(image, 1, (255,0,0), rho, angles_array_3)

  print("timeline: ", str(time.time() - time_line))

  return angle_out


# What model to download.
# MODEL_NAME = '/media/ivliev/9016-4EF8/airport_all/workspace/training_demo/trained-inference-graphs/output_inference_graph_v1.pb'
MODEL_NAME = '/home/ivliev/airport_all/workspace/training_demo/trained-inference-graphs/output_inference_graph_v3.pb'
PATH_TO_CKPT = MODEL_NAME + '/frozen_inference_graph.pb'
PATH_TO_LABELS = os.path.join('annotations', 'label_map.pbtxt')
NUM_CLASSES = 3


# ## Load a (frozen) Tensorflow model into memory.
detection_graph = tf.Graph()
with detection_graph.as_default():
  od_graph_def = tf.GraphDef()
  with tf.gfile.GFile(PATH_TO_CKPT, 'rb') as fid:
    serialized_graph = fid.read()
    od_graph_def.ParseFromString(serialized_graph)
    tf.import_graph_def(od_graph_def, name='')


# ## Loading label map
label_map = label_map_util.load_labelmap(PATH_TO_LABELS)
categories = label_map_util.convert_label_map_to_categories(label_map, max_num_classes=NUM_CLASSES, use_display_name=True)
category_index = label_map_util.create_category_index(categories)


# ## Helper code
def load_image_into_numpy_array(image):
  (im_width, im_height) = image.size
  return np.array(image.getdata()).reshape(
      (im_height, im_width, 3)).astype(np.uint8)



with detection_graph.as_default():
  with tf.Session(graph=detection_graph) as sess:
    while True:
      time_1 = time.time()

      ret, image_np = cap.read()
    
      image_np_expanded = np.expand_dims(image_np, axis=0)
      image_tensor = detection_graph.get_tensor_by_name('image_tensor:0')
      # Each box represents a part of the image where a particular object was detected.
      boxes = detection_graph.get_tensor_by_name('detection_boxes:0')
      # Each score represent how level of confidence for each of the objects.
      # Score is shown on the result image, together with the class label.
      scores = detection_graph.get_tensor_by_name('detection_scores:0')
      classes = detection_graph.get_tensor_by_name('detection_classes:0')
      num_detections = detection_graph.get_tensor_by_name('num_detections:0')
      # Actual detection.
      (boxes, scores, classes, num_detections) = sess.run(
          [boxes, scores, classes, num_detections],
          feed_dict={image_tensor: image_np_expanded})

      # print(boxes, scores, classes, num_detections)

      tag_array = []
      barcode_array = []

      # print("len(boxes) : ",len(boxes[0]))
      for i in range(len(boxes[0])):
        if scores[0][i] > 0.5:
          if classes[0][i] == 1:
            tag_array.append(boxes[0][i])
          if classes[0][i] == 3:
            barcode_array.append(boxes[0][i])

        else:
          break



      w = image_np.shape[1]
      h = image_np.shape[0]

      for barcode in range(len(barcode_array)):
        i = barcode
        print(boxes[0][i])
        rect = [int(boxes[0][i][0] * h), 
                int(boxes[0][i][1] * w), 
                int(boxes[0][i][2] * h), 
                int(boxes[0][i][3] * w)]
        
        print(rect)

        start_point = (rect[1], rect[0]) 
        end_point = (rect[3], rect[2]) 
        color = (0, 0, 255) 
        thickness = 2

        barcode_frame = image_np[rect[0]:rect[2], rect[1]:rect[3]]   
        # cv2.imwrite('barcode_frame.jpg',barcode_frame)
        # image_np = cv2.rectangle(image_np, start_point, end_point, color, thickness) 

        # bar_code(barcode_frame)
        angle_out = line_detaction(barcode_frame)
        angle = angle_out * 180 / np.pi

        (h, w) = image_np.shape[:2] 
        center = (w // 2, h // 2)
        M = cv2.getRotationMatrix2D(center, angle, 1.0)
        print(M)
        # rotated = cv2.warpAffine(image_np, M, (w, h),flags=cv2.INTER_CUBIC, borderMode=cv2.BORDER_REPLICATE)
        try:
          rotated = cv2.warpAffine(image_np, M, ( int(w / np.cos(angle_out)), int(h / np.sin(angle_out)) ) )
          cv2.imshow('rotated '+str(i)+'', cv2.resize(rotated, (400,300)))

        except:
          print()


      vis_util.visualize_boxes_and_labels_on_image_array(
          image_np,
          np.squeeze(boxes),
          np.squeeze(classes).astype(np.int32),
          np.squeeze(scores),
          category_index,
          use_normalized_coordinates=True,
          line_thickness=3)

          # cv2.imwrite('/media/ivliev/F045-E632/image_np'+str(i)+'.jpg',image_np)

      print(image_np.shape)
      print(time.time()-time_1)

      cv2.imshow('object detection', cv2.resize(image_np, (800,600)))
      if cv2.waitKey(25) & 0xFF == ord('q'):
        cv2.destroyAllWindows()
        break
